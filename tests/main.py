"""Test"""
import asyncio
import logging

from wp_imazu.client import ImazuClient
from wp_imazu.packet import ImazuPacket

logging.basicConfig(level=logging.DEBUG)

if __name__ == '__main__':
    async def async_device_state_handler(imazu_packet: ImazuPacket):
        """Device state handler"""
        logging.debug(imazu_packet.description())


    loop = asyncio.get_event_loop()
    sock = ImazuClient("192.168.100.30", 8899)
    sock.async_packet_handler = async_device_state_handler
    loop.create_task(sock.async_connect())
    loop.run_forever()
